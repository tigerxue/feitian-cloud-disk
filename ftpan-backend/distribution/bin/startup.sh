#!/usr/bin/env sh

ftpan_HOME=`cd $(dirname $0)/..; pwd`

JAVA_OPTS="${JAVA_OPTS} -Dftpan.homeDir=${ftpan_HOME}"
JAVA_OPTS="${JAVA_OPTS} -jar "${ftpan_HOME}/lib/ftpan-web.jar""
JAVA_OPTS="${JAVA_OPTS} --spring.config.location=${ftpan_HOME}/conf/application.properties"

if [[ ! -d "${ftpan_HOME}/data/logs" ]]; then
    mkdir -p ${ftpan_HOME}/data/logs
fi

nohup java ${JAVA_OPTS} >> ${ftpan_HOME}/data/logs/ftpan.log 2>&1 &
echo "started ftpan!!!"
#java ${JAVA_OPTS}