#!/usr/bin/env sh

ftpan_HOME=`cd $(dirname $0)/..; pwd`

JAVA_OPTS="${JAVA_OPTS} -Dftpan.homeDir=${ftpan_HOME}"

#############################################################
# Mysql configuration
#############################################################
if [[ $USE_MYSQL == true ]]; then
    JAVA_OPTS="${JAVA_OPTS} -Dftpan.useMysql=true"
    JAVA_OPTS="${JAVA_OPTS} -Dftpan.mysql.url=jdbc:mysql://${MYSQL_HOST}:${MYSQL_PORT}/${MYSQL_DB_NAME}"
    JAVA_OPTS="${JAVA_OPTS} -Dftpan.mysql.username=${MYSQL_USERNAME}"
    JAVA_OPTS="${JAVA_OPTS} -Dftpan.mysql.password=${MYSQL_PASSWORD}"
fi

JAVA_OPTS="${JAVA_OPTS} -jar "${ftpan_HOME}/lib/ftpan-web.jar""
JAVA_OPTS="${JAVA_OPTS} --spring.config.location=${ftpan_HOME}/conf/application.properties"

if [ ! -d "${ftpan_HOME}/data/logs" ]; then
    mkdir -p ${ftpan_HOME}/data/logs
fi

java ${JAVA_OPTS}