#!/usr/bin/env bash

pid=`ps aux | grep "ftpan-web.jar" | grep -v grep|awk '{print $2}'`
if [ -z $pid ]; then
    echo "no ftpan server running..."
    exit -1
fi

echo "kill ftpan server..."
kill $pid
echo "success kill server at pid:${pid}"