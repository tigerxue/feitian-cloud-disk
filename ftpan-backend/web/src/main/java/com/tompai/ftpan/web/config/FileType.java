package com.tompai.ftpan.web.config;

/**
 * @author tompai
 * @date 2020-01-11
 */
public enum FileType {
	/**
	 * 文件类型
	 */
	DEFAULT, 
	FOLDER, 
	VIDEO, 
	AUDIO, 
	PDF, 
	COMPRESS_FILE, 
	PICTURE, 
	DOC, 
	PPT, 
	TXT, 
	TORRENT, 
	WEB, 
	CODE,
	UNKNOWN;

	@Override
	public String toString() {
		return name().toLowerCase();
	}
}
