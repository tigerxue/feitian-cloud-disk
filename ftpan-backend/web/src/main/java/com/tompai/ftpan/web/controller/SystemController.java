package com.tompai.ftpan.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tompai.ftpan.web.config.Constants;
import com.tompai.ftpan.web.dto.ResponseDto;
import com.tompai.ftpan.web.service.SystemService;

/**
 * @author tompai
 * @date 2020-02-10
 */
@RestController
@RequestMapping(Constants.API_VERSION)
public class SystemController {

	private final SystemService systemService;

	@Autowired
	public SystemController(SystemService systemService) {
		this.systemService = systemService;
	}

	@GetMapping("/system")
	public ResponseEntity<ResponseDto> getSystemInfo() {
		return ResponseEntity.ok(ResponseDto.success(systemService.systemInfo()));
	}
}
