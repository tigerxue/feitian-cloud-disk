package com.tompai.ftpan.web.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author tompai
 * @date 2020-01-22
 */
@Setter
@Getter
public class MoveAndCopyFileDto {

	public static final String MOVE_TYPE = "move";
	public static final String COPY_TYPE = "copy";

	private List<Long> fileIds;
	private List<Long> targetIds;
	private String type;
}
