package com.tompai.ftpan.web.dto;

import com.tompai.ftpan.web.entity.File;

import lombok.Getter;
import lombok.Setter;

/**
 * @author tompai
 * @date 2020-01-27
 */
@Setter
@Getter
public class MergeFileDto extends File {

	private String identifier;
}
