package com.tompai.ftpan.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author tompai
 * @date 2020-01-11
 */
@Setter
@Getter
@AllArgsConstructor
public class FolderPathDto {

	private Long id;
	private String fileName;
}
