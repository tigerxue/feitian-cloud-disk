package com.tompai.ftpan.web.service;

import com.tompai.ftpan.web.dto.MergeFileDto;
import com.tompai.ftpan.web.entity.ResourceChunk;

/**
 * @author tompai
 * @date 2020-01-27
 */
public interface ResourceService {

	/**
	 * 检查块是否已经上传
	 * @param chunk 块
	 * @return true： 已经上传了 false：还未上传
	 */
	boolean checkChunk(ResourceChunk chunk);

	/**
	 * 保存chunk
	 * @param chunk chunk
	 */
	void saveChunk(ResourceChunk chunk);

	/**
	 * 合并chunk
	 * @param mergeFileDto 文件信息
	 */
	void mergeChunk(MergeFileDto mergeFileDto);

}
