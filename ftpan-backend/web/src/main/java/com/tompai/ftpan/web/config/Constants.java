package com.tompai.ftpan.web.config;

/**
 * @author tompai
 * @date 2020-01-05
 */
public class Constants {

	public static final String API_VERSION = "/v1";

	public static final String PND_HOME = "feifian.homeDir";

	public static final String USE_MYSQL = "feifian.useMysql";
	public static final String MYSQL_URL = "feifian.mysql.url";
	public static final String MYSQL_USERNAME = "feifian.mysql.username";
	public static final String MYSQL_PASSWORD = "feifian.mysql.password";

	public static final String MAX_FILE_UPLOAD_SIZE = "feifian.max.uploadFile.size";
	public static final String MAX_REQUEST_SIZE = "feifian.max.request.size";
}
