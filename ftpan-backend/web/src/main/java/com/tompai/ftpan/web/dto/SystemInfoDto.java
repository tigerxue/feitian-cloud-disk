package com.tompai.ftpan.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author tompai
 * @date 2020-02-10
 */
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SystemInfoDto {

	private Long totalCap;
	private Long usableCap;

	private Integer totalNum;
	private Integer folderNum;
	private Integer fileNum;

	private Integer videoNum;
	private Integer audioNum;

}
