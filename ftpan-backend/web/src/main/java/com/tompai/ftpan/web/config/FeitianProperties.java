package com.tompai.ftpan.web.config;

import java.io.File;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.tompai.ftpan.web.util.FeitianStringUtils;
import com.tompai.ftpan.web.util.FeitianUtils;

import lombok.Getter;
import lombok.Setter;

/**
 * @author tompai
 * @date 2020-01-10
 */
@Getter
@Setter
@Configuration
public class FeitianProperties {

	/**
	 * mysql config
	 */
	private boolean useMysql;
	private String mysqlUrl;
	private String mysqlUsername;
	private String mysqlPassword;
	private String mysqlDriver = "com.mysql.cj.jdbc.Driver";

	private String embedDbDriver = "org.apache.derby.jdbc.EmbeddedDriver";
	private String embedDbUrl;
	private String embedDbUsername = "feitian";
	private String embedDbPassword = "feitian";

	private String pndHome;
	private String pndDataDir;

	private String maxFileUploadSize;
	private String maxRequestSize;

	private final Environment env;

	@Autowired
	public FeitianProperties(Environment env) {
		this.env = env;
	}

	@PostConstruct
	public void init() {
		setPndHome(getEnvProperty(Constants.PND_HOME,
				getSystemProperty("user.home", FeitianStringUtils.EMPTY) + File.separator + "FTPan"));
		setPndDataDir(getPndHome() + File.separator + "data");
		setEmbedDbUrl("jdbc:derby:" + getPndDataDir() + File.separator + "db;create=true");

		setUseMysql(Boolean.valueOf(getEnvProperty(Constants.USE_MYSQL, "false")));
		setMysqlUrl(getEnvProperty(Constants.MYSQL_URL, FeitianStringUtils.EMPTY));
		setMysqlUsername(getEnvProperty(Constants.MYSQL_USERNAME, FeitianStringUtils.EMPTY));
		setMysqlPassword(getEnvProperty(Constants.MYSQL_PASSWORD, FeitianStringUtils.EMPTY));

		setMaxFileUploadSize(getEnvProperty(Constants.MAX_FILE_UPLOAD_SIZE, "100MB"));
		setMaxRequestSize(getEnvProperty(Constants.MAX_REQUEST_SIZE, "200MB"));
	}

	public String getBasicResourcePath() {
		String basicPath = getPndDataDir() + File.separator + "resources";
		FeitianUtils.createFolders(basicPath);
		return basicPath;
	}

	public String getResourceTmpDir() {
		String resourceTmpPath = getBasicResourcePath() + File.separator + "tmp";
		FeitianUtils.createFolders(resourceTmpPath);
		return resourceTmpPath;
	}

	private String getEnvProperty(String key, String defaultVal) {
		String val = env.getProperty(key, "");
		if (FeitianStringUtils.isBlank(val)) {
			return defaultVal;
		}
		return val;
	}

	private static String getSystemProperty(String key, String defaultVal) {
		String val = System.getProperty(key, "");
		if (FeitianStringUtils.isBlank(val)) {
			return defaultVal;
		}
		return val;
	}
}
