package com.tompai.ftpan.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author tompai
 * @date 2020-01-05
 */
@SpringBootApplication
public class FeitianWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeitianWebApplication.class, args);
	}

	/*@Profile("dev")
	@Configuration
	static class DefaultWebMvcConfigurer implements WebMvcConfigurer {
		@Override
		public void addCorsMappings(CorsRegistry registry) {
			registry.addMapping("/**").allowedMethods("*");
		}
	}*/
}
