package com.tompai.ftpan.web.entity;

import java.util.Date;

import org.apache.ibatis.type.Alias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author tompai
 * @date 2020-01-08
 */
@Alias("resource")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Resource {
	public static final String ALIAS = "resource";
	private Long id;
	private Long size;
	private String md5;
	private String path;
	@Builder.Default
	private Integer link = 0;

	@Builder.Default
	private Date createTime = new Date();
	@Builder.Default
	private Date updateTime = new Date();
}
