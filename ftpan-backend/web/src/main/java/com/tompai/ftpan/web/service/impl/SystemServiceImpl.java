package com.tompai.ftpan.web.service.impl;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tompai.ftpan.web.config.FileType;
import com.tompai.ftpan.web.config.FeitianProperties;
import com.tompai.ftpan.web.dao.FileMapper;
import com.tompai.ftpan.web.dto.SystemInfoDto;
import com.tompai.ftpan.web.service.SystemService;

/**
 * @author tompai
 * @date 2020-02-10
 */
@Service
public class SystemServiceImpl implements SystemService {

	private final FileMapper fileMapper;
	private final FeitianProperties feitianProperties;

	@Autowired
	public SystemServiceImpl(FileMapper fileMapper, FeitianProperties feitianProperties) {
		this.fileMapper = fileMapper;
		this.feitianProperties = feitianProperties;
	}

	@Override
	public SystemInfoDto systemInfo() {
		SystemInfoDto.SystemInfoDtoBuilder builder = SystemInfoDto.builder();
		File systemFile = new File(feitianProperties.getPndDataDir());
		builder.totalCap(systemFile.getTotalSpace()).usableCap(systemFile.getUsableSpace());

		List<com.tompai.ftpan.web.entity.File> files = fileMapper.getAllFileType();

		int totalFileNum = 0, folderNum = 0, videoNum = 0, audioNum = 0;
		for (com.tompai.ftpan.web.entity.File file : files) {
			totalFileNum++;
			if (FileType.FOLDER.toString().equals(file.getType())) {
				folderNum++;
			}
			if (FileType.VIDEO.toString().equals(file.getType())) {
				videoNum++;
			}
			if (FileType.AUDIO.toString().equals(file.getType())) {
				audioNum++;
			}
		}

		builder.totalNum(totalFileNum).fileNum(totalFileNum - folderNum).folderNum(folderNum).videoNum(videoNum)
				.audioNum(audioNum);
		return builder.build();
	}
}
