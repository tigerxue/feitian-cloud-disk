package com.tompai.ftpan.web.service;

import com.tompai.ftpan.web.dto.SystemInfoDto;

/**
 * @author tompai
 * @date 2020-02-10
 */
public interface SystemService {

	/**
	 * 系统信息
	 * @return SystemInfoDto
	 */
	SystemInfoDto systemInfo();
}
