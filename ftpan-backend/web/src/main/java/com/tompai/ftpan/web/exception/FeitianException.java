package com.tompai.ftpan.web.exception;

/**
 * @author tompai
 * @date 2020-01-11
 */
public class FeitianException extends RuntimeException {
	private static final long serialVersionUID = -455084301379506105L;

	public FeitianException() {
		super();
	}

	public FeitianException(String message) {
		super(message);
	}

	public FeitianException(String message, Throwable cause) {
		super(message, cause);
	}

	public FeitianException(Throwable cause) {
		super(cause);
	}

	protected FeitianException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
