## 介绍
`ftpan`全名为`FeiTian Pan Network Disk`，为了实现一个部署在家庭、寝室等私人生活中或者小范围团队的网络存储空间，是一个具有从远程服务器后台下载到个人机器、家庭文件共享、在线影视观看等功能的方便、快捷的生活助手。

[前端VUE页面](https://gitee.com/tigerxue/feitian-cloud-disk/tree/master/ftpan-web)

## 功能
- [x] 文件及文件夹：增/删/重命名/移动/复制
- [x] 大文件的分块上传，支持文件上传的暂停/恢复，上传进度展示
- [x] 文件秒传
- [ ] 重复文件检测
- [ ] 音乐在线播放
- [ ] 视频在线播放
- [ ] 文件的批量打包下载
- [ ] 简单文件的在线浏览
- [ ] 简单文件的在线编辑
- [ ] websocket聊天功能
- [ ] ......

## 下载源码或安装包

### 直接下载二进制包
[选择最新的二进制包](https://gitee.com/tigerxue/feitian-cloud-disk/releases)

### 部署(推荐)

``` sh
java -jar ftpan-web.jar

# 浏览器访问：http://<ip>:8989
```

### 源码构建

``` sh
git clone git@gitee.com:tigerxue/feitian-cloud-disk.git

cd ftpan
mvn clean package

# ftpan/distribution/target" 下可看到构建好的 *.tar.gz 和 *.zip 包
# 运行
bin/startup.sh

# 浏览器访问：http://localhost:8989
```
